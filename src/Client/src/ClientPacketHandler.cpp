#include "ClientPacketHandler.h"
#include "Services.h"
#include "Client.h"

bool PacketHandlerClient::HandleMessage(packet_id_type aID, rak_peer_ptr aPeer, packet_ptr aPacket)
{
	switch (aID)
	{
		// Handle client packets
	case ID_ALREADY_CONNECTED:
	{
		printf("Already connected with guid %s", aPacket->guid);
		return true;
	}
	case ID_CONNECTION_BANNED:
	{
		printf("We are banned from this server");
		return true;
	}
	case ID_CONNECTION_ATTEMPT_FAILED:
	{
		printf("Connection to server failed");
		return true;
	}
	case ID_INVALID_PASSWORD:
	{
		printf("Password INVALID");
		return true;
	}
	case ID_CONNECTION_REQUEST_ACCEPTED:
	{
		printf("\nConnection require ACCEPTED to %s with GUID %s", aPacket->systemAddress.ToString(true), aPacket->guid.ToString());
		printf("\nMy external address is %s", aPeer->GetExternalID(aPacket->systemAddress).ToString(true));
		return true;
	}

	case packet_id::ID_BALL_STATE:
	{
		BulletInfo bi = *(BulletInfo*)aPacket->data;
		
		mBulletInfo = bi;

		return true;
	}

	case packet_id::ID_ASTERIOD_STATE:
	{
		auto itr = std::find_if(mAsteriodInfo.begin(), mAsteriodInfo.end(), [aPacket](const AsteroidInfo& aAI)
		{
			if (aPacket->guid == aAI.mGUID)
				return true;
			return false;
		});


		if (itr == mAsteriodInfo.end())
		{
			AsteroidInfo temp;
			temp.mGUID = aPacket->guid;
			mAsteriodInfo.push_back(temp);

			itr = mAsteriodInfo.end() - 1;
		}

		AsteroidInfo ai = *(AsteroidInfo*)aPacket->data;
		itr->mGUID = ai.mGUID;
		itr->mHeight = ai.mHeight;
		itr->mOriginX = ai.mOriginX;
		itr->mOriginY = ai.mOriginY;
		itr->mPacketID = ai.mPacketID;
		itr->mRotation = ai.mRotation;
		itr->mWidth = ai.mWidth;
		itr->mX = ai.mX;
		itr->mY = ai.mY;

		return true;
	}

	case packet_id::ID_PLAYER_STATE:
	{
		auto itr = std::find_if(mPlayerInfo.begin(), mPlayerInfo.end(), [aPacket](const PlayerInfo& aPI)
		{
			if (aPacket->guid == aPI.mGUID)
				return true;
			return false;
		});

		if (itr == mPlayerInfo.end())
		{
			PlayerInfo temp;
			temp.setGUID(aPacket->guid);
			mPlayerInfo.push_back(temp);
			itr = mPlayerInfo.end() - 1;
		}

		// mouse positioning
		MousePos mp = *(MousePos*)aPacket->data;
		itr->setState(mp);

		return true;
	}

	case packet_id::ID_NUM_OF_CLIENTS:
	{
		if (!mPosSet)
		{
			mPosSet = true;
			NumOfClients noc = *(NumOfClients*)aPacket->data;

			// So this works except the numOfPlayers aka mPlayerInfo.size() from the server isn't consistantly returning the correct amount.
			if (noc.numOfPlayers % 2) // If it's odd
				Services::getEventSystem()->fireEvent(Event(EventType::RIGHT_SIDE));
			else
				Services::getEventSystem()->fireEvent(Event(EventType::LEFT_SIDE));

			printf("\nNum: %i", (int)noc.numOfPlayers);
			if (noc.numOfPlayers >= 1)
			{
				Services::getEventSystem()->fireEvent(Event(EventType::GAME_START));
				// Still need to send a message about it to the other client... so maybe not only an event make a packet and send it.
			}
		}
		return true;
	}

	case packet_id::ID_START_GAME:
	{
		Services::getEventSystem()->fireEvent(Event(EventType::GAME_START));
		return true;
	}

	default:
	{
		printf("random packet: ");
		return true;
	}

	}

	return false;
}