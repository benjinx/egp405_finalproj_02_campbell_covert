#include "Server.h"
#include "RakPeerInterface.h"
#include <iostream>

Server::Server(port_type aPort)
: mPort(aPort)
{
	mPeer = RakNet::RakPeerInterface::GetInstance();
	mpPacketHandler = new PacketHandlerServer(mPeer);
}

Server::~Server()
{
	delete mpPacketHandler;
	mpPacketHandler = nullptr;

	mPeer->Shutdown(300);
	RakNet::RakPeerInterface::DestroyInstance(mPeer);
}

bool Server::launch() const
{
	mPeer->SetIncomingPassword(0, 0);

	// IPV4 Socket
	RakNet::SocketDescriptor sd;
	sd.port = mPort;
	sd.socketFamily = AF_INET;

	if (mPeer->Startup(4, &sd, 1) != RakNet::RAKNET_STARTED)
	{
		printf("\nFailed to start server with IPV4 ports.");
		return false;
	}

	mPeer->SetOccasionalPing(true);
	mPeer->SetUnreliableTimeout(1000);
	mPeer->SetMaximumIncomingConnections(4);

	printf("\nServer IP Address: ");
	for (unsigned int i = 0; i < mPeer->GetNumberOfAddresses(); i++)
	{
		RakNet::SystemAddress sa = mPeer->GetInternalID(RakNet::UNASSIGNED_SYSTEM_ADDRESS, i);
		printf("%i. %s (LAN=%i)", i+1, sa.ToString(false), sa.IsLANAddress());
	}
	return true;
}

void Server::preUpdate(secType aDeltaT)
{
	if (mpPacketHandler)
		mpPacketHandler->update(aDeltaT);
}

void Server::printUsage()
{
	printf("\n-- Usage for Server --");
	printf("\nServer <serverPort>\n");
}