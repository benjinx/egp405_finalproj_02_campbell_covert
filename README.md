# README #
Asteroids!

### Team Members ###
* Benjamin Campbell
* Daniel Covert

### Repository Location ###
https://bitbucket.org/benjinx/egp405_finalproj_02_campbell_covert

### Game Description ###

* Shoot the amazing asteroids with your best friend!


### Controls ###

* Player movement is controled via the mouse.
* Fire using the left mouse button.


### Set Up ###

* This project will need to be assembled using CMake in 32bit.
* After creating the project, open up the solution and compile it in either debug or release depending on your needs.
* Once everything has been compiled just run the .bat files and fill in the required information for each. (Server first then clients)