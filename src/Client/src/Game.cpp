#include "Game.h"
#include "GraphicsSystem.h"
#include "InputSystem.h"
#include "Services.h"
#include "Client.h"

Game* Game::mspGameInstance = NULL;

Game::Game()
	:EventListener()
	, mIsInitialized(false)
	, mIsGameRunning(false)
	, mReadyToBegin(false)
{
	


	mEventSystem.addListener(EventType::GAME_EXIT, this);

	setRemoveOnDestruct(false); // Game shouldn't auto remove itself from the event system.
}


Game::~Game()
{
	cleanup();
}


bool Game::init()
{
	if (mIsInitialized)
		return false;

	// Set width and height here.
	int displayWidth = 960, displayHeight = 540;

	// Initalize the systems.
	if (!mGraphicsSystem.init("My Game", displayWidth, displayHeight))
		return false;
	if (!mInputSystem.init())
		return false;

	// Provide systems to services
	Services::init();
	Services::provideGraphicsSystem(&mGraphicsSystem);
	Services::provideInputSystem(&mInputSystem);
	Services::provideEventSystem(&mEventSystem);

	// Timing
	mCurrentTime = 0.0;
	mPreviousTime = 0.0;
	mFpsUpdateTime = 0.0;

	//Wait 
	mWaitTexture = new sf::Texture;
	mWaitTexture->loadFromFile("WaitScreen.jpg");
	mWaitSprite = new sf::Sprite();
	mWaitSprite->setTexture(*mWaitTexture);
	mWaitSprite->setPosition((float)((mGraphicsSystem.getWidth() / 2) - 250), (float)((mGraphicsSystem.getHeight() - 500) / 2));

	// Load Assets
	const std::string PATH = "../Assets/";

	mpBackgroundTexture = new sf::Texture;
	//mpBackgroundTexture->loadFromFile(PATH + "PongBackground.png");

	mpBackgroundSprite = new sf::Sprite();
	mpBackgroundSprite->setTexture(*mpBackgroundTexture);
	mpBackgroundSprite->setPosition(0, 0);
	
	mIsInitialized = true;
	return true;
}

void Game::cleanup()
{
	if (mIsInitialized)
	{
		// Shutdown systems.
		mGraphicsSystem.cleanup();
		mInputSystem.cleanup();
		Services::cleanup();

		// Release assets
		delete mpBackgroundSprite;
		delete mpBackgroundTexture;
		delete mWaitSprite;
		delete mWaitTexture;


		// Not initialized anymore.
		mIsInitialized = false;
	}
}

void Game::gameLoop(Client* apClient)
{
	// Game is running... so true
	mIsGameRunning = true;

	// Create clock and set fps.
	
	mCurrentFps = 60;

	// Main Game loop.
	while (mGraphicsSystem.getWindow()->isOpen() && mIsGameRunning)
	{ 
		mCurrentTime = mClock.getElapsedTime().asSeconds();
		mDeltaTime = mCurrentTime - mPreviousTime;

		// FrameRate
		mFpsUpdateTime += mDeltaTime;

		// Handle the packets.

		apClient->preUpdate(mDeltaTime);

		// Get Input
		mInputSystem.pollInput();

		// Update
		update(mDeltaTime);

		// Draw
		mReadyToBegin = apClient->hasGameStarted();
		draw();

		// Draw Networked objects
		apClient->postUpdate();

		// Flip Display
		mGraphicsSystem.flipDisplay();

		mPreviousTime = mCurrentTime;
	}
}

void Game::update(float deltaTime)
{
	
}

void Game::draw()
{
	// Draw background
	mGraphicsSystem.drawSprite(*mpBackgroundSprite, sf::Rect<int>(sf::Vector2<int>(0, 0), sf::Vector2<int>(960, 540)), false);

	if (!mReadyToBegin)
	{
		mGraphicsSystem.drawSprite(*mWaitSprite, sf::Rect<int>(sf::Vector2<int>(0,0), sf::Vector2<int>(500, 500)), false);
	}

	// Draw the walls
	/*mTopWall.setSize(sf::Vector2f(760, 10));
	mTopWall.setPosition(sf::Vector2f(100, 20));
	mGraphicsSystem.drawShape(mTopWall);

	mBottomWall.setSize(sf::Vector2f(760, 10));
	mBottomWall.setPosition(sf::Vector2f(100, 510));
	mGraphicsSystem.drawShape(mBottomWall);*/
}

void Game::endGame()
{
	mIsGameRunning = false;
}

void Game::handleEvent(const Event& theEvent)
{
	if (theEvent.getType() == EventType::GAME_EXIT)
		endGame();
}