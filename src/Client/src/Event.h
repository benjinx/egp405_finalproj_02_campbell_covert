#ifndef EVENT_H
#define EVENT_H

#include <string>


enum EventType
{
	INVALID_EVENT_TYPE = -1,

	// Game
	MOVE_UP,			// Move Right
	MOVE_DOWN,	// Move left
	DEBUG,				// Debug menu
	GAME_EXIT,			// Exit the game.
	GAME_START,
	RIGHT_SIDE,
	LEFT_SIDE,
	FIRE_BULLET,

	NUM_EVENT_TYPES
};


class Event
{
public:
	Event(EventType type) :
		mType(type)
	{

	}
	virtual ~Event() {}

	EventType getType() const { return mType; };

private:
	EventType mType;
};


#endif // EVENT_H