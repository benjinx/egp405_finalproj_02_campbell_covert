#include <iostream>
#include <memory>
/*

	THIS IS THE MAIN FOR SERVER CURRENTLY


*/
#include "Server.h"
#include "SFML/System/Clock.hpp"

int main(int argc, char** argv)
{
	typedef float secType;

	if (argc == 2)
	{
		const int portNum = atoi(argv[1]);
		std::unique_ptr<Server> pServer(new Server(portNum));
		pServer->launch();

		sf::Clock clock;

		while (1) // this should be like shouldexit
		{
			secType aDeltaT = clock.restart().asSeconds();

			pServer->preUpdate(aDeltaT);
		}
	}
	else
		Server::printUsage(); 
	
	system("pause");
	return EXIT_SUCCESS;
}