#ifndef CLIENT_H
#define CLIENT_H

#include <iostream>
#include <algorithm>
#include <memory>
#include "../../common/src/PacketHandler.h"
#include "../../common/src/GamePacketHandler.h"
#include "RakPeerInterface.h"
#include "SFML/System/Clock.hpp"
#include "SFML/Graphics.hpp"
#include "Event.h"
#include "EventListener.h"
#include "ClientPacketHandler.h"

namespace RakNet { class RakPeerInterface; };

class Client : public EventListener
{
public:
	typedef unsigned short			  port_type;
	typedef RakNet::RakPeerInterface* rak_peer_interface_ptr;
	typedef float				  secType;

	Client(port_type aPort);
	~Client();
	bool Connect(const char* aSeverAddress, port_type aServerPort) const;

	port_type getPort() { return mPort; }
	rak_peer_interface_ptr getPeer() { return mPeer; }
	void preUpdate(secType aDeltaT);
	void postUpdate();

	// Mutators
	void setShapePosition(float x, float y) { mMyShape.setPosition(x, y); }

	// Events
	void handleEvent(const Event& theEvent);

	// help menu
	static void printUsage();
	static void startingGame(bool isStarting);

	//For wait menu
	bool hasGameStarted(){ return mGameStarted; }
	
private:
	port_type			   mPort;
	rak_peer_interface_ptr mPeer;
	GamePacketHandler_I* mpPacketHandler;
	sf::CircleShape mMyShape;
	std::vector<sf::CircleShape> mOtherShips; // THIS IS THE OTHER SPACESHIP(S) INCASE WE WANTED MORE THAN JUST ONE OPPONENT!
	sf::RectangleShape mMyBullet, mOtherBullet;
	std::vector<sf::RectangleShape> mAsteriodsLocal;
	sf::Vector2f mBulletDirection;
	sf::Vector2f mAsteroidDirection;
	bool mGameStarted;
};

#endif CLIENT_H