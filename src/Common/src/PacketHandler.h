#ifndef PACKET_HANDLER_H
#define PACKET_HANDLER_H

#include "Packet.h"
#include "SFML/System/Clock.hpp"

namespace RakNet
{
	class  RakPeerInterface;
	struct Packet;
};

class PacketHandler_I
{
public:
	typedef PacketHandler_I			  this_type;
	typedef RakNet::RakPeerInterface* rak_peer_ptr;
	typedef RakNet::Packet*			  packet_ptr;
	typedef unsigned char			  packet_id_type;
	typedef float				  sec_type;

	PacketHandler_I(rak_peer_ptr aPeer);

	void update(sec_type aDeltaTime);

	rak_peer_ptr getPeer() { return mPeer; }

protected:
	void HandlePackets(sec_type aDeltaTime, rak_peer_ptr aPeer);

	virtual void Pre_HandlePackets(sec_type aDeltaTime, rak_peer_ptr aPeer);
	virtual bool HandleMessage(packet_id_type aID, rak_peer_ptr aPeer, packet_ptr aPacket) = 0;
	virtual void Post_HandlePackets(sec_type aDeltaTime, rak_peer_ptr aPeer);

	virtual void DoHandlePackets(sec_type aDeltaTime, rak_peer_ptr aPeer);

private:
	rak_peer_ptr mPeer;
};

#endif PACKET_HANDLER_H