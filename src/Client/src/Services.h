#ifndef SERVICES_H
#define SERVICES_H

#include "GraphicsSystem.h"
#include "InputSystem.h"
#include "EventSystem.h"
#include "Client.h"
//#include "Server.h"

class Services
{
public:
	inline static Services* getInstance() { return mspInstance; }
	static bool init();
	static void cleanup();

	// Service Access
	inline static GraphicsSystem* getGraphicsSystem() { return getInstance()->mpGraphicsSystem; }
	inline static InputSystem* getInputSystem() { return getInstance()->mpInputSystem; }
	//inline static Client* getClient() { return getInstance()->mpClient; }
	//inline static Server* getServer() { return getInstance()->mpServer; }
	inline static EventSystem* getEventSystem() { return getInstance()->mpEventSystem; }

	// Service provision
	inline static void provideGraphicsSystem(GraphicsSystem* pGraphics) { getInstance()->mpGraphicsSystem = pGraphics; }
	inline static void provideInputSystem(InputSystem* pInputSystem) { getInstance()->mpInputSystem = pInputSystem; }
	//inline static void provideClient(Client* pClient) { getInstance()->mpClient = pClient; }
	//inline static void provideServer(Server* pServer) { getInstance()->mpServer = pServer; }
	inline static void provideEventSystem(EventSystem* pEventSystem) { getInstance()->mpEventSystem = pEventSystem; }

private:
	Services();
	~Services(){};

	static Services* mspInstance;

	// Services
	GraphicsSystem* mpGraphicsSystem;
	InputSystem* mpInputSystem;
	//Client* mpClient;
	//Server* mpServer;
	EventSystem* mpEventSystem;
};

#endif