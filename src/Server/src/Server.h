#ifndef SERVER_H
#define SERVER_H

#include "../../common/src/PacketHandler.h"
#include "../../common/src/GamePacketHandler.h"
#include "SFML/System/Clock.hpp"
#include <memory>
#include <algorithm>
#include "RakPeer.h"

namespace RakNet { class RakPeerInterface; };

class PacketHandlerServer : public GamePacketHandler_I
{
public:
	PacketHandlerServer(rak_peer_ptr aPeer)
		: GamePacketHandler_I(aPeer)
	{ }

	bool HandleMessage(packet_id_type aID, rak_peer_ptr aPeer, packet_ptr aPacket)
	{
		switch (aID)
		{
		case ID_NEW_INCOMING_CONNECTION:
		{
			printf("\nNew incoming connection from %s w/ GUID %s", aPacket->systemAddress.ToString(true), aPacket->guid.ToString());
			
			NumOfClients noc;
			noc.mPacketID = packet_id::ID_NUM_OF_CLIENTS;
			noc.numOfPlayers = mPlayerInfo.size();
			noc.mGUID = aPeer->GetMyGUID();
			aPeer->Send((const char*)&noc, sizeof(NumOfClients), HIGH_PRIORITY, UNRELIABLE_SEQUENCED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
			return true;
		}
		case ID_CONNECTION_LOST:
		case ID_CONNECTION_BANNED:
		case ID_DISCONNECTION_NOTIFICATION:
		{
			auto itr = std::find_if(mPlayerInfo.begin(), mPlayerInfo.end(), [aPacket](const PlayerInfo& aPI)
			{
				if (aPacket->guid == aPI.mGUID)
					return true;
				return false;
			});

			//if (itr != mPlayerInfo.end())
			//{
				printf("\nPlayer disconnected: ", itr->mGUID.ToString());
				mPlayerInfo.erase(itr);
			//}

			return true;
		}

		case packet_id::ID_BALL_STATE:
		{
			BulletInfo bi = *(BulletInfo*)aPacket->data;
			mBulletInfo = bi;

			// broadcast the state to everybody EXCEPT the client who sent us the
			// state
			aPeer->Send((const char*)&bi, sizeof(BulletInfo), HIGH_PRIORITY,
				UNRELIABLE_SEQUENCED, 0, aPacket->systemAddress, true);
			return true;
		}

		case packet_id::ID_ASTERIOD_STATE:
		{
			auto itr = std::find_if(mAsteriodInfo.begin(), mAsteriodInfo.end(), [aPacket](const AsteroidInfo& aAI)
			{
				if (aPacket->guid == aAI.mGUID)
					return true;
				return false;
			});


			if (itr == mAsteriodInfo.end())
			{
				AsteroidInfo temp;
				temp.mGUID = aPacket->guid;
				mAsteriodInfo.push_back(temp);

				itr = mAsteriodInfo.end() - 1;
			}

			AsteroidInfo ai = *(AsteroidInfo*)aPacket->data;
			itr->mGUID = ai.mGUID;
			itr->mHeight = ai.mHeight;
			itr->mOriginX = ai.mOriginX;
			itr->mOriginY = ai.mOriginY;
			itr->mPacketID = ai.mPacketID;
			itr->mRotation = ai.mRotation;
			itr->mWidth = ai.mWidth;
			itr->mX = ai.mX;
			itr->mY = ai.mY;

			// broadcast the state to everybody EXCEPT the client who sent us the
			// state
			aPeer->Send((const char*)&ai, sizeof(AsteroidInfo), HIGH_PRIORITY,
				UNRELIABLE_SEQUENCED, 0, aPacket->systemAddress, true);

			return true;
		}

		case packet_id::ID_PLAYER_STATE:
		{
			auto itr = std::find_if(mPlayerInfo.begin(), mPlayerInfo.end(), [aPacket](const PlayerInfo& aPI)
			{
				if (aPacket->guid == aPI.mGUID)
					return true;
				return false;
			});


			if (itr == mPlayerInfo.end())
			{
				PlayerInfo temp;
				temp.setGUID(aPacket->guid);
				mPlayerInfo.push_back(temp);

				itr = mPlayerInfo.end() - 1;
			}

			MousePos mp = *(MousePos*)aPacket->data;
			itr->setState(mp);

			// broadcast the state to everybody EXCEPT the client who sent us the
			// state
			aPeer->Send((const char*)&mp, sizeof(MousePos), HIGH_PRIORITY,
				UNRELIABLE_SEQUENCED, 0, aPacket->systemAddress, true);

			return true;
		}

		case packet_id::ID_START_GAME:
		{
			GameStarting gs = *(GameStarting*)aPacket->data;
			aPeer->Send((const char*)&gs, sizeof(GameStarting), HIGH_PRIORITY,
				UNRELIABLE_SEQUENCED, 0, aPacket->systemAddress, true);
			return true;
		}

		}

		return true;
	}
};

class Server
{
public:
	typedef unsigned short			  port_type;
	typedef RakNet::RakPeerInterface* rak_peer_interface_ptr;
	typedef float				  secType;

	Server(port_type aPort);
	~Server();
	bool launch() const;

	//void setPort(port_type aPort) { mPort = aPort; }
	//void setPeer(rak_peer_interface_ptr aPeer) { mPeer = aPeer; }
	port_type getPort() { return mPort; }
	rak_peer_interface_ptr getPeer() { return mPeer; }
	void preUpdate(secType aDeltaTime);
	static void printUsage();

private:
	port_type			   mPort;
	rak_peer_interface_ptr mPeer;
	PacketHandlerServer* mpPacketHandler;
};

#endif SERVER_H