#ifndef GAME_H
#define GAME_H

#include <assert.h>
#include <stddef.h>
#include "SFML/System/Time.hpp"
#include "RakPeerInterface.h"
#include "GraphicsSystem.h"
#include "InputSystem.h"
#include "Client.h"
#include "Event.h"
#include "EventListener.h"
#include "EventSystem.h"
#include "SFML/Graphics.hpp"

class Game : public EventListener
{
public:

	typedef float secType;

	// Singleton
	static Game* getInstance()		{ assert(mspGameInstance != NULL); return mspGameInstance; }
	static void  initInstance()		{ mspGameInstance = new Game(); }
	static void  cleanupInstance()	{ delete mspGameInstance; }

	bool init();
	void cleanup();
	void gameLoop(Client* apClient);
	void update(float deltaTime);
	void draw();
	void endGame();

	float getCurrentTime() { return mCurrentTime; }

	sf::RectangleShape getTopWall() { return mTopWall; }
	sf::RectangleShape getBottomWall() { return mBottomWall; }

	// Events
	void handleEvent(const Event& theEvent);

private:
	Game();
	~Game();
	static Game* mspGameInstance;

	bool mIsInitialized;
	bool mIsGameRunning;

	//Wait Screen Vars
	bool mReadyToBegin;
	sf::Texture* mWaitTexture;
	sf::Sprite* mWaitSprite;



	// Systems
	GraphicsSystem mGraphicsSystem;
	InputSystem mInputSystem;
	EventSystem mEventSystem;
	
	// Timing
	int mCurrentFps;
	int	mFrameCounter;
	float mCurrentTime, mDeltaTime, mPreviousTime, mFpsUpdateTime;
	sf::Clock mClock;

	// Assets
	sf::Texture* mpBackgroundTexture;
	sf::Sprite* mpBackgroundSprite;

	sf::RectangleShape mTopWall, mBottomWall;
};

#endif