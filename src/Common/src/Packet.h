#ifndef PACKET_H
#define PACKET_H

#include "MessageIdentifiers.h"
#include "RakNetTypes.h"

namespace RakNet{ struct Packet; }

namespace packet_id
{
	enum {
		ID_PLAYER_STATE = ID_USER_PACKET_ENUM,
		ID_NUM_OF_CLIENTS,
		ID_BALL_STATE,
		ID_START_GAME,
		ID_ASTERIOD_STATE
	};
};

#pragma pack(push, 1)
struct MousePos
{
public:
	typedef unsigned char packet_id_type;

	packet_id_type mPacketID;
	int mX, mY;
	float mOriginX, mOriginY;
	float mRotation;
	RakNet::RakNetGUID mGUID;
};
#pragma pack(pop)

#pragma pack(push, 1)
struct ShapeInfo
{
public:
	typedef unsigned char packet_id_type;

	packet_id_type mPacketID;
	float mX, mY, mWidth, mHeight;
	RakNet::RakNetGUID mGUID;
};
#pragma pack(pop)

#pragma pack(push, 1)
struct NumOfClients
{
public:
	typedef unsigned char packet_id_type;

	packet_id_type mPacketID;
	int numOfPlayers;
	RakNet::RakNetGUID mGUID;
};
#pragma pack(pop)

#pragma pack(push, 1)
struct GameStarting
{
public:
	typedef unsigned char packet_id_type;

	packet_id_type mPacketID;
	bool gameStarting;
	RakNet::RakNetGUID mGUID;
};
#pragma pack(pop)

#pragma pack(push, 1)
struct BulletInfo
{
public:
	typedef unsigned char packet_id_type;
	packet_id_type mPacketID;
	float mX, mY, mWidth, mHeight, mRotation, mOriginX, mOriginY;
	RakNet::RakNetGUID mGUID;
};
#pragma pack(pop)

#pragma pack(push, 1)
struct AsteroidInfo
{
	typedef unsigned char packet_id_type;
	packet_id_type mPacketID;
	float mX, mY, mWidth, mHeight, mRotation, mOriginX, mOriginY;
	RakNet::RakNetGUID mGUID;
};
#pragma pack(pop)

namespace f_packet
{
	unsigned char getPacketID(RakNet::Packet* p);
};

#endif PACKET_H