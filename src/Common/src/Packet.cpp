#include "Packet.h"
#include "RakNetTypes.h"

namespace f_packet
{
	unsigned char getPacketID(RakNet::Packet* p)
	{
		return p->data[0];
	}
};