#include "Client.h"
#include "Game.h"
#include "Services.h"
#include "SFML/Window.hpp"

Client::Client(port_type aPort)
: EventListener()
, mPort(aPort)
{
	mPeer = RakNet::RakPeerInterface::GetInstance();
	mpPacketHandler = new PacketHandlerClient(mPeer);
	Services::getEventSystem()->addListener(EventType::MOVE_UP, this);
	Services::getEventSystem()->addListener(EventType::MOVE_DOWN, this);
	Services::getEventSystem()->addListener(EventType::RIGHT_SIDE, this);
	Services::getEventSystem()->addListener(EventType::LEFT_SIDE, this);
	Services::getEventSystem()->addListener(EventType::GAME_START, this);
	Services::getEventSystem()->addListener(EventType::FIRE_BULLET, this);
	setRemoveOnDestruct(false); // Client shouldn't auto remove itself from the event system, that would be bad.
	
	// My shape info
	mMyShape.setRadius(50);
	mMyShape.setPointCount(3);
	mMyShape.setPosition(80, 270 - (mMyShape.getRadius() / 2));
	mMyShape.setFillColor(sf::Color(100, 250, 50));

	// Asteriods
	for (int i = 0; i < 5; i++)
	{
		sf::RectangleShape tempAsteriod;
		tempAsteriod.setSize(sf::Vector2f(100, 100));
		tempAsteriod.setPosition(sf::Vector2f(100.0f*i, 150.0f*i));
		tempAsteriod.setFillColor(sf::Color(222, 184, 135));
		mAsteriodsLocal.push_back(tempAsteriod);
	}

	/*
		// Populate bricks on left
	for (int i = 0; i < 24; ++i)
	{
		mOtherShapes.push_back(sf::RectangleShape());
		mOtherShapes[i].setFillColor(sf::Color::Green);
		mOtherShapes[i].setSize(sf::Vector2f(11.0f, 36.0f));
		mOtherShapes[i].setPosition(sf::Vector2f((float)(20 + (40 * (i % 2))), (float)(30 + (40 * (i / 2)))));
	}

	// Populate bricks on right
	for (int i = 24; i < 48; ++i)
	{
		mOtherShapes.push_back(sf::RectangleShape());
		mOtherShapes[i].setFillColor(sf::Color::Red);
		mOtherShapes[i].setSize(sf::Vector2f(11.0f, 36.0f));
		mOtherShapes[i].setPosition(sf::Vector2f((float)(890 + (40 * ((i - 24) % 2))), (float)(30 + (40 * ((i - 24) / 2)))));
	}*/

	mBulletDirection.x = 0;
	mBulletDirection.y = 0;

	mAsteroidDirection.x = 1;
	mAsteroidDirection.y = 1;

	mGameStarted = false;
}

Client::~Client()
{
	delete mpPacketHandler;
	mpPacketHandler = nullptr;

	mPeer->Shutdown(300);
	RakNet::RakPeerInterface::DestroyInstance(mPeer);
}

bool Client::Connect(const char* aServerAddress, port_type aServerPort) const
{
	// IPV4 socket
	RakNet::SocketDescriptor sd(mPort, 0);
	sd.socketFamily = AF_INET;

	mPeer->Startup(8, &sd, 1);
	mPeer->SetOccasionalPing(true);

	if (mPeer->Connect(aServerAddress, aServerPort, 0, 0) != RakNet::CONNECTION_ATTEMPT_STARTED)
	{
		printf("Attempt to connect to server FAILED.");
		return false;
	}

	printf("\nClient IP Address: ");
	for (unsigned int i = 0; i < mPeer->GetNumberOfAddresses(); i++)
		printf("%i. %s\n", i+1, mPeer->GetLocalIP(i));

	return true;
}

void Client::preUpdate(secType aDeltaT)
{
	if (mpPacketHandler)
		mpPacketHandler->update(aDeltaT);

	for (int i = 0; i < 5; i++)
	{
		// Asteroids
		AsteroidInfo ai;
		ai.mPacketID = packet_id::ID_ASTERIOD_STATE;
		ai.mX = mAsteriodsLocal.at(i).getPosition().x;
		ai.mY = mAsteriodsLocal.at(i).getPosition().y;
		ai.mWidth = mAsteriodsLocal.at(i).getSize().x;
		ai.mHeight = mAsteriodsLocal.at(i).getSize().y;
		ai.mOriginX = mAsteriodsLocal.at(i).getOrigin().x;
		ai.mOriginY = mAsteriodsLocal.at(i).getOrigin().x;
		ai.mRotation = mAsteriodsLocal.at(i).getRotation();
		ai.mGUID = getPeer()->GetMyGUID();
		getPeer()->Send((const char*)&ai, sizeof(AsteroidInfo), HIGH_PRIORITY, UNRELIABLE_SEQUENCED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
	}

	// Math for players info
	auto pos = sf::Mouse::getPosition(*Services::getInstance()->getGraphicsSystem()->getWindow());
	float rotation = 0.0f;

	auto direction = (sf::Vector2f)pos - mMyShape.getPosition();
	auto XY = mMyShape.getPosition() + (direction * aDeltaT);
	auto x = XY.x;
	auto y = XY.y;

	mMyShape.setOrigin(mMyShape.getRadius(), mMyShape.getRadius());

	rotation = (float)(atan2(mMyShape.getPosition().y - pos.y, mMyShape.getPosition().x - pos.x) * (180 / 3.14159265));

	// Pack up info and send it
	MousePos mp;
	mp.mPacketID = packet_id::ID_PLAYER_STATE;
	mp.mX = (int)x;
	mp.mY = (int)y;
	mp.mOriginX = mMyShape.getOrigin().x;
	mp.mOriginY = mMyShape.getOrigin().y;
	mp.mRotation = rotation;
	mp.mGUID = getPeer()->GetMyGUID();
	getPeer()->Send((const char*)&mp, sizeof(MousePos), HIGH_PRIORITY, UNRELIABLE_SEQUENCED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);

	// Create otherships
	while (mpPacketHandler->getPlayerInfo().size() > mOtherShips.size())
	{
		sf::CircleShape newShape(50, 3);
		newShape.setFillColor(sf::Color(sf::Color::Blue));
		mOtherShips.push_back(newShape);
		
		// create other bullet
		sf::RectangleShape newBullet(sf::Vector2f(10, 10));
		newBullet.setFillColor(sf::Color(sf::Color::Blue));
		mOtherBullet = newBullet;
	}

	/*while (mpPacketHandler->getAsteroidInfo().size() > mAsteriodsLocal.size())
	{
		ai.mPacketID = packet_id::ID_ASTERIOD_STATE;
		ai.mX = mAsteriodsLocal.at(i).getPosition().x;
		ai.mY = mAsteriodsLocal.at(i).getPosition().y;
		ai.mWidth = mAsteriodsLocal.at(i).getSize().x;
		ai.mHeight = mAsteriodsLocal.at(i).getSize().y;
		ai.mOriginX = mAsteriodsLocal.at(i).getOrigin().x;
		ai.mOriginY = mAsteriodsLocal.at(i).getOrigin().x;
		ai.mRotation = mAsteriodsLocal.at(i).getRotation();
		ai.mGUID = getPeer()->GetMyGUID();
	}*/

	// Setup the bullet
	mMyBullet.setSize(sf::Vector2f(10, 10));
	mMyBullet.setOrigin(mMyBullet.getSize().x / 2, mMyBullet.getSize().y / 2);
	mMyBullet.setFillColor(sf::Color(sf::Color::Green));

	// Pack up bullet info and send it
	BulletInfo bi;
	bi.mPacketID = packet_id::ID_BALL_STATE;
	bi.mX = mMyBullet.getPosition().x;
	bi.mY = mMyBullet.getPosition().y;
	bi.mWidth = mMyBullet.getSize().x;
	bi.mHeight = mMyBullet.getSize().y;
	bi.mOriginX = mMyBullet.getOrigin().x;
	bi.mOriginY = mMyBullet.getOrigin().y;
	bi.mRotation = mMyBullet.getRotation();
	bi.mGUID = getPeer()->GetMyGUID();
	getPeer()->Send((const char*)&bi, sizeof(BulletInfo), HIGH_PRIORITY, UNRELIABLE_SEQUENCED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
	
	//printf("%f\n", bi.mX);

	if (mGameStarted)
	{

		// Should use this stuff to makesure position is more correctly set but it's fine right now...
		auto directionSQ = direction.x * direction.x + direction.y * direction.y;
		float normalizedDir = 1.0f / (sqrt(directionSQ) + FLT_MIN);
		//


		mMyShape.setPosition(mMyShape.getPosition() + (direction * aDeltaT));
		mMyShape.setRotation(rotation + 33);
		mMyBullet.setPosition((mMyBullet.getPosition().x + mBulletDirection.x), (mMyBullet.getPosition().y + mBulletDirection.y));
		
		for (unsigned i = 0; i < mAsteriodsLocal.size(); i++)
		{
			/*
			AND HERE'S WHERE I'D PUT COLLISION, IF I HAD ANY!!!!!!!! Q.Q
			
			if (mAsteriodsLocal.at(i).getLocalBounds().intersects(mMyBullet.getLocalBounds()))
			{
				printf("DED\n");
			}*/

			if (mAsteriodsLocal.at(i).getPosition().x < 0 || mAsteriodsLocal.at(i).getPosition().x >= Services::getGraphicsSystem()->getWidth())
			{
				mAsteriodsLocal.at(i).setPosition(sf::Vector2f(50.0f * i, 50.0f * i));
				mAsteroidDirection.x = 1;
				mAsteroidDirection.y = 1;
			}
			else if (mAsteriodsLocal.at(i).getPosition().y < 0 || mAsteriodsLocal.at(i).getPosition().y >= Services::getGraphicsSystem()->getHeight())
			{
				mAsteriodsLocal.at(i).setPosition(sf::Vector2f(100.0f * i, 100.0f * i));
				mAsteroidDirection.x = 1;
				mAsteroidDirection.y = -1;
			}
			mAsteriodsLocal.at(i).setPosition(sf::Vector2f(mAsteriodsLocal.at(i).getPosition().x + mAsteroidDirection.x, (mAsteriodsLocal.at(i).getPosition().y + mAsteroidDirection.y)));
		}
	}
}

void Client::postUpdate()
{
	// Draw mMyShape
	Services::getGraphicsSystem()->drawShape(mMyShape);

	// Draw bullet
	Services::getGraphicsSystem()->drawShape(mMyBullet);


	for (int i = 0; i < 5; i++)
	{
		mAsteriodsLocal.at(i).setPosition(sf::Vector2f(mAsteriodsLocal.at(i).getPosition().x , mAsteriodsLocal.at(i).getPosition().y ));
		Services::getGraphicsSystem()->drawShape(mAsteriodsLocal.at(i));
	}

	// Draw opponent
	for (unsigned int i = 0; i < mpPacketHandler->getPlayerInfo().size(); i++)
	{
		mOtherShips[i].setPosition((float)(mpPacketHandler->getPlayerInfoAt(i).mState.mX), (float)(mpPacketHandler->getPlayerInfoAt(i).mState.mY));
		mOtherShips[i].setOrigin(mpPacketHandler->getPlayerInfoAt(i).mState.mOriginX, mpPacketHandler->getPlayerInfoAt(i).mState.mOriginY);
		mOtherShips[i].setRotation(mpPacketHandler->getPlayerInfoAt(i).mState.mRotation + 33);
		Services::getGraphicsSystem()->drawShape(mOtherShips[i]);

		// Draw opponent bullet
		mOtherBullet.setPosition((float)mpPacketHandler->getBulletInfo().mX, (float)mpPacketHandler->getBulletInfo().mY);
		Services::getGraphicsSystem()->drawShape(mOtherBullet);
	}


	

}

void Client::handleEvent(const Event& theEvent)
{
	if (theEvent.getType() == EventType::MOVE_UP)
	{
		if (mMyShape.getPosition().y > 40)
		mMyShape.setPosition(mMyShape.getPosition().x, mMyShape.getPosition().y - 20);
	}
	else if (theEvent.getType() == EventType::MOVE_DOWN)
	{
		if (mMyShape.getPosition().y < 420)
		mMyShape.setPosition(mMyShape.getPosition().x, mMyShape.getPosition().y + 20);
	}
	else if (theEvent.getType() == EventType::RIGHT_SIDE)
	{
		printf("\nRight");
		mMyShape.setPosition(mMyShape.getPosition().x + 760, mMyShape.getPosition().y);
	}
	else if (theEvent.getType() == EventType::LEFT_SIDE)
	{
		printf("\nLeft");
		mMyShape.setPosition(mMyShape.getPosition().x, mMyShape.getPosition().y);
	}
	else if (theEvent.getType() == EventType::GAME_START) // Start moving the ball here.. or maybe call a bool and set to true and have an update elsewhere
	{
		printf("START!");
		if (!mGameStarted)
		{
			printf("Sent");
			GameStarting gs;
			gs.mPacketID = packet_id::ID_START_GAME;
			gs.gameStarting = true;
			gs.mGUID = getPeer()->GetMyGUID();
			getPeer()->Send((const char*)&gs, sizeof(GameStarting), HIGH_PRIORITY, UNRELIABLE_SEQUENCED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
		}
		mGameStarted = true;
		//mBallShape.setPosition(mBallShape.getPosition().x, mBallShape.getPosition().y);
	}
	else if (theEvent.getType() == EventType::FIRE_BULLET)
	{
		mMyBullet.setRotation(mMyShape.getRotation());
		mMyBullet.setPosition(mMyShape.getPosition().x, mMyShape.getPosition().y);
		
		if (mMyShape.getRotation() <= 33 || mMyShape.getRotation() >= 300)
		{
			mBulletDirection.x = -2;
			mBulletDirection.y = 2;
		}
		else if (mMyShape.getRotation() >= 34 && mMyShape.getRotation() <= 120)
		{
			mBulletDirection.x = -2;
			mBulletDirection.y = -2;
		}
		else if (mMyShape.getRotation() >= 121 && mMyShape.getRotation() <= 210)
		{
			mBulletDirection.x = 2;
			mBulletDirection.y = -2;
		}
		else if (mMyShape.getRotation() >= 211 && mMyShape.getRotation() <= 299)
		{
			mBulletDirection.x = 2;
			mBulletDirection.y = 2;
		}
		
	}
}

void Client::printUsage()
{
	printf("\n-- USAGE --");
	printf("\nClient <serverIP> <serverPort> <clientPort>\n");
}

void Client::startingGame(bool isStarting)
{

}