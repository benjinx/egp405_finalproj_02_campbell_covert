#ifndef GAME_PACKET_HANDLER_H
#define GAME_PACKET_HANDLER_H

#include "PacketHandler.h"
#include "PlayerInfo.h"
#include <vector>

class GamePacketHandler_I : public PacketHandler_I
{
public:
	typedef std::vector<PlayerInfo> playerInfoCont;
	typedef std::vector<AsteroidInfo> asteriodInfoCont;

	GamePacketHandler_I(rak_peer_ptr aPeer);

	PlayerInfo getPlayerInfoAt(int location) { return mPlayerInfo.at(location); }
	std::vector<PlayerInfo> getPlayerInfo() { return mPlayerInfo; }
	BulletInfo getBulletInfo() { return mBulletInfo; }
	AsteroidInfo getAsteroidInfoAt(int loc) { return mAsteriodInfo.at(loc); }
	std::vector<AsteroidInfo> getAsteroidInfo() { return mAsteriodInfo; }

protected:
	playerInfoCont mPlayerInfo;
	BulletInfo mBulletInfo;
	asteriodInfoCont mAsteriodInfo;
};

#endif GAME_PACKET_HANDLER_H