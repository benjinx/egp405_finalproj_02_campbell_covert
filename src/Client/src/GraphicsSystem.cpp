#include "GraphicsSystem.h"
#include "Services.h"


GraphicsSystem::GraphicsSystem()
{
	mIsInitialized = false;
}

GraphicsSystem::~GraphicsSystem()
{
	cleanup();
}

bool GraphicsSystem::init(const std::string& displayTitle, int displayWidth, int displayHeight)
{
	if (mIsInitialized)
		return false;

	// Set width & height
	mWidth = displayWidth;
	mHeight = displayHeight;

	sf::ContextSettings contextSettings;
	contextSettings.depthBits = 24;

	// Setup window
	mpWindow = new sf::RenderWindow(sf::VideoMode(mWidth, mHeight), displayTitle, sf::Style::Default, contextSettings);
	mpWindow->setActive();

	//mpFont = new Font("../assets/porky.ttf", 16);

	// Mark as Inited
	mIsInitialized = true;
	return true;
}

void GraphicsSystem::cleanup()
{
	if (mIsInitialized)
	{
		//SDL_DestroyTexture(mpSDLTexture);
		//delete mpFont;
		//SDL_DestroyRenderer(mpSDLRenderer);
		//SDL_DestroyWindow(mpSDLWindow);
		//SDL_Quit();
		delete mpWindow;
		mIsInitialized = false;
	}
}

// Clear the display to a single color.
void GraphicsSystem::clearToColor(sf::Color color)
{
	// Sets the color on the renderer & fills it with the color
	mpWindow->clear(color);
}

// Swap the display buffers.
void GraphicsSystem::flipDisplay()
{
	// Update and draw to the screen
	mpWindow->display();

	// Clear the display.
	mpWindow->clear(); // Might have to remove?
}

void GraphicsSystem::drawSprite(const sf::Sprite& sprite, const sf::Rect<int> destinationRect, bool shouldFlip)
{
	// Render sprite to the screen
	mpWindow->draw(sprite);
}

void GraphicsSystem::drawColoredSprite(const sf::Sprite& sprite, const sf::Rect<int> rect, sf::Color& color, bool shouldFlip)
{
	// Render sprite to the screen
	mpWindow->draw(sprite);
}

void GraphicsSystem::drawShape(sf::CircleShape aShape)
{
	mpWindow->draw(aShape);
}

void GraphicsSystem::drawShape(sf::RectangleShape aShape)
{
	mpWindow->draw(aShape);
}

void GraphicsSystem::writeText(const std::string& message, int size, sf::Color& color, sf::Vector2f& location, bool shouldFlip)
{
	//sf::Text text;
	//text.setFont(mpFont->getFont());
	//text.setString(message);
	//text.setCharacterSize(size);
	//text.setPosition(location);
	//text.setColor(color); figure this out

	//mpWindow->draw(text);
}

void GraphicsSystem::writeText(const std::string& message, int size, sf::Color& color, int x, int y, bool shouldFlip)
{
	//sf::Text text;
	//text.setFont(mpFont->getFont());
	//text.setString(message);
	//text.setCharacterSize(size);

	//sf::Vector2f tempLocation(x, y);
	//text.setPosition(tempLocation);
	//text.setColor(color); figure this out

	//mpWindow->draw(text);
}